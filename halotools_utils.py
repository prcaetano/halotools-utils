from __future__ import print_function
import argparse
import numpy as np
import os
import sys
import time

from itertools import product
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

from halotools.empirical_models import PrebuiltHodModelFactory, Zheng07Cens, Zheng07Sats, get_halo_mass_key
from halotools.sim_manager import CachedHaloCatalog
from halotools.mock_observables import mock_survey
from halotools.mock_observables import tpcf, tpcf_one_two_halo_decomp, tpcf_jackknife
from halotools.mock_observables import angular_tpcf

from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.stats import lognorm

from jack_tools import calculate_jackknife_xi_covariance2

#Default hod parameters
hodpars = {
           "sigmam": 0.43,
           "massmin": 11.82,
           "alpha": 1.15,
           "mass0": 11.02,
           "mass1": 13.01,
           "fcentral": 0.5
          }

def populate_halos(hodpars, sim='bolshoi', sim_redshift=0, halo_finder='rockstar', fname=None, model=None,
                   mdef="vir", conc_mass_model="direct_from_halo_catalog"):
    """
    Creates synthetic galaxy sample by populating halos of a simulation.

    Parameters:
        hodpars: dict
            describes hod parameters. Should have the keys sigmam, massmin, alpha, mass0 and mass1, fcentral
        r_bins: np.ndarray
            array containing distances to calc cf on (in Mpc)
        sim: str
            name of simulation from where the halo catalog was taken.
        sim_redshift: float
            redshift of simulation slice used
        halo_finder: str
            halo finder used to produce halo catalog
        fname: str
            path to hd5 preprocessed halo catalog. If provided, sim, sim_redshift and halo_finder should be ignored.
        model:
            hod model with precomputed stuff to speed up mock generation
        mdef: str
            halo definition to use (e.g., "vir" for virial and "200c" for 200 times the critical density)
        conc_mass_model: str
            concentration model of NFW profile. Can be "direct_from_halo_catalog" (default) or "dutton_maccio14"

    Returns:
        gals, model.mock

        gals: astropy.Table
            galaxy population simulated
        model:
            model object
    """
    mass_key = get_halo_mass_key(mdef)
    if model is None:
        model = PrebuiltHodModelFactory('zheng07', modulate_with_cenocc=True,
                                        conc_mass_model=conc_mass_model, mdef=mdef)
        ntries = 10
        for try_ in range(ntries):
            try:
                if fname is None:
                    halocat = CachedHaloCatalog(simname=sim, redshift=sim_redshift,
                                            halo_finder=halo_finder)
                else:
                    halocat = CachedHaloCatalog(fname=fname)
            except IOError:
                if try_ == ntries-1:
                    raise
            else:
                break
        model.populate_mock(halocat, halo_mass_column_key=mass_key)

    model.param_dict['alpha'] = hodpars['alpha']
    model.param_dict['logM0'] = hodpars['mass0']
    model.param_dict['logM1'] = hodpars['mass1']
    model.param_dict['logMmin'] = hodpars['massmin']
    model.param_dict['sigma_logM'] = hodpars['sigmam']
    fcentral = hodpars['fcentral']

    model.mock.populate()
    gals = model.mock.galaxy_table

    idx_centrals, = np.nonzero(gals['gal_type'] == 'centrals')
    idx_satellites, = np.nonzero(gals['gal_type'] == 'satellites')
    n_centrals_selected = max(int(fcentral * len(idx_centrals)), len(idx_centrals))
    try:
        idx_centrals_selected = np.random.choice(idx_centrals, n_centrals_selected, replace=False)
    except ValueError:
        idx_centrals_selected = np.array([])
    idx = np.append(idx_centrals_selected, idx_satellites)
    gals_selected = gals[idx]

    return gals_selected, model

def calculate_mock_xi(gals, rmin, rmax, nbins, Lbox, njacksubdivisions=5,
                      nrandoms=None, return_halo_decomp=True, return_covariance=False):
    """
    Calculates xi(r) for mocks.

    Parameters:
        gals: astropy.Table
            synthetic galaxy population
        rmin, rmax: float
            minimum and maximum value of separation, in Mpc, to calculate correlation
        nbins: int
            number of log spaced bins to use to calculate correlation
        Lbox: float or np.ndarray of shape (3,)
            size of box of simulation (in Mpc/h)
        njacksubdivisions: int
            number of subdivisions on box to make when creating jackknife samples, that it,
            the number of jackknife samples is njacksubdivisions**3
        nrandoms: int
            size of random sample to use. Defauts to 15 times the galaxy sample
        return_halo_decomp: bool (default: True)
            if True, also calculates 1 and 2 halo correlations xi_1h and xi_2h
        return_covariance: bool (default: False)
            if True, also calculates covariance for total correlation function

    Returns:
        r, xi, xi_1h, xi_2h, cov (if return_halo_decomp and return_covariance)
        r, xi, cov               (if return_covariance and not return_halo_decomp)
        r, xi, xi_1h, xi_2h      (if return_halo_decomp and not return covariance)
        r, xi                    (otherwise)


        r: np.ndarray
            array with the centers of the bins used
        xi: np.ndarray
            array with calculated two point correlation function
        xi_1h: np.ndarray
            array with calculated 1 halo two point correlation function
        xi_2h: np.ndarray
            array with calculated 2 halo two point correlation function
        cov: np.ndarray
            array with shape (len(r)-1,len(r)-1) with covariance of xi
    """
    r_bins = np.logspace(np.log10(rmin), np.log10(rmax), nbins)
    coords = np.vstack([gals['x'], gals['y'], gals['z']]).T
    halo_hostid = gals['halo_hostid']

    # asserts r_bins is inside simulation box
    r_bins = r_bins[r_bins < Lbox[0]/3.]

    if return_covariance:
        if nrandoms is None:
            nrandoms = len(gals)*15
        randoms = np.random.random((nrandoms,3))*Lbox

        r, xi, cov = calculate_jackknife_xi_covariance2(coords, randoms, rmin, rmax, nbins, Lbox,
                                                        njacksubdivisions)
        #xi, cov = tpcf_jackknife(coords, randoms, r_bins, Nsub=2, period=Lbox, num_threads='max')
    else:
        xi = tpcf(coords, r_bins, period=Lbox, num_threads='max')
        r = (r_bins[:-1] + r_bins[1:])/2.0

    if return_halo_decomp:
        xi_1h, xi_2h = tpcf_one_two_halo_decomp(coords, halo_hostid, r_bins,
                                                period=Lbox,
                                                num_threads='max')
        if return_covariance:
            return r, xi, xi_1h, xi_2h, cov
        else:
            return r, xi, xi_1h, xi_2h
    else:
        if return_covariance:
            return r, xi, cov
        else:
            return r, xi


def calculate_mock_acf(theta_bins, Lbox, gals=None, ra=None, dec=None, z=None, nrandoms=1e5):
    """
    Calculates acf for mocks.

    Parameters:
        theta_bins: np.ndarray
            array containing angles to calc acf on (in Mpc)
        Lbox: float or np.ndarray of shape (3,)
            size of box of simulation (in Mpc/h)
        gals: astropy.Table
            synthetic galaxy population (optional, needed if ra, dec or z missing)
        ra: np.ndarray
            array with ra of galaxy sample to measure (optional, needed if gals not specified)
        dec: np.ndarray
            array with dec of galaxy sample to measure (optional, needed if gals not specified)
        z: np.ndarray
            array with redshift of galaxy sample to measure (optional, needed if gals not specified)
        nrandoms: int
            size of random sample. Defaults to 1e5.

    Returns:
        theta, acf, catalg

        theta: np.ndarray
            array with center of the bins used
        acf: np.ndarray
            array with calculated acf
        catalog: np.ndarray
            array with columns ra, dec and z of simulated catalog (angles in degrees)
    """
    if (ra is None or dec is None or z is None):
        coords = np.vstack([gals['x'], gals['y'], gals['z']]).T
        vels = np.vstack([gals['vx'], gals['vy'], gals['vz']]).T

        ra, dec, z = mock_survey.ra_dec_z(coords, vels)
        ra = np.degrees(ra)
        dec = np.degrees(dec)

    ran_coords = np.random.random((nrandoms,3))*Lbox
    ran_vels = np.zeros((nrandoms,3))

    ran_ra, ran_dec, ran_z = mock_survey.ra_dec_z(ran_coords, ran_vels)
    ran_ra = np.degrees(ran_ra)
    ran_dec = np.degrees(ran_dec)

    angular_coords = np.vstack((ra,dec)).T
    ran_angular_coords = np.vstack((ran_ra, ran_dec)).T

    theta = (theta_bins[:-1] + theta_bins[1:])/2.
    acf = angular_tpcf(angular_coords, theta_bins, randoms=ran_angular_coords, num_threads='max')

    catalog = np.c_[ra, dec, z]

    return theta, acf, catalog


def simulate_xi(hod, r, xi,
                sim="bolshoi", sim_redshift=0., halo_finder='rockstar',
                fname=None, mdef="vir", conc_mass_model="direct_from_halo_catalog",
                model=None, n_realizations=1,
               ):
    """
    Simulate n_realizations of the correlation function, evaluated at distances r, for hod parameters hod.

    Parameters:
        hod: dict
            hod parameters.
        r: np.ndarray
            values of distance measured (Mpc/h)
        xi: np.ndarray
            corresponding values of spatial correlation.
        sim: str
            name of simulation to use
        sim_redshift: str
            redshifts of halo catalog to use.
        halo_finder: str, optional
            halo finder used to create halo catalog.
        fname: str
            path to file holding preprocessed halo catalog hdf5 file.
        conc_mass_model: str, optional
            concentration model for NFW profile. Can be "direct_from_halo_catalog" or "dutton_maccio14".
        mdef: str
            halo definition to use (e.g., "vir" for virial and "200c" for 200 times the critical density)
        model:
            hod model with precomputed stuff to speed up mock generation
        n_realizations: int
            number of realizations of galaxy population to use. Defaults to 1.

    Returns: xis_r, model
        xis_r: np.ndarray, shape (n_realizations, len(r))
            array containing on each row the values of the simulated correlation function for all r
        model:
            hod model with precomputed stuff to speed up mock generation
    """
    # Ugly hack to make range of mean r for bins to cover original range
    r_min, r_max = r.min(), r.max()
    nr_bins = int( (np.log10(r_max) - np.log10(r_min)) * 5 )  # 5 bins per decade
    bin_size = (np.log10(r_max) - np.log10(r_min)) / nr_bins
    logr_min = np.log10(r_min) - bin_size
    logr_max = np.log10(r_max) + bin_size
    bin_size += 2
    r_bins = np.logspace(logr_min, logr_max, nr_bins)

    xis_r = []
    for _ in range(n_realizations):
        if fname is None:
            gals, model = populate_halos(hodpars=hod, sim=sim, sim_redshift=sim_redshift,
                                         halo_finder=halo_finder, model=model, mdef=mdef,
                                         conc_mass_model=conc_mass_model)
        else:
            gals, model = populate_halos(hodpars=hod, fname=fname, model=model, mdef=mdef,
                                         conc_mass_model=conc_mass_model)

        coords = np.vstack([gals['x'], gals['y'], gals['z']]).T
        del(gals)
        Lbox = model.mock.Lbox

        # asserts r_bins is inside simulation box
        r_bins = r_bins[r_bins < Lbox[0]/3.]

        xi_hod = tpcf(coords, r_bins, period=Lbox, num_threads='max')
        r_hod = (r_bins[:-1] + r_bins[1:])/2.0

        xi_interp_log = interp1d(np.log(r_hod), np.log(xi_hod))
        xi_interp_linear = interp1d(r_hod, xi_hod)
        xi_r = np.exp(xi_interp_log(np.log(r)))
        nan_mask = np.isnan(xi_r)
        xi_r[nan_mask] = xi_interp_linear(r)[nan_mask]
        xis_r.append(xi_r)
        del(r_hod, xi_hod, xi_r, nan_mask)
    xis_r = np.array(xis_r)
    return xis_r, model


def generate_mocks(hodpars, r_min, r_max, nr_bins, theta_min, theta_max, ntheta_bins,
                   data_dir, nrandoms=int(1e5),
                   sim="bolshoi", sim_redshifts=None, halo_finder='rockstar',
                   fnames=None, mdef="vir", conc_mass_model="direct_from_halo_catalog",
                   calculate_xi=True, calculate_acf=True, calculate_covariance=False,
                   njacksubdivisions=5):
    """
    Generate mock population for all hod parameters on hodpars and saves galaxy population,
    spatial correlation function and angular correlation function to data_dir directory.

    Parameters:
        hodpars: list of dict
            list of hod parameters.
        r_min, r_max: float
            minimum and maximum of separation interval to calculate correlation function
            (in Mpc)
        nr_bins: int
            number of logscale bins to calculate correlation function
        theta_min, theta_max: float
            minimum and maximum of angular separation to calculate correlation function
            (in degrees)
        ntheta_bins: int
            number of logscale bins to calculate angular correlation function
        data_dir: str
            path to directory to save mock data
        nrandoms: integer
            size of random sample used to calculate acf
        sim: str
            name of simulation to use
        sim_redshifts: list of str
            list of redshifts of halo catalogues to use for each value of hod parameters. Should have the same length as hodpars.
        halo_finder: str, optional
            halo finder used to create halo catalog.
        fnames: str, optional
            paths to files holding preprocessed halo catalogs hdf5 files.
        conc_mass_model: str, optional
            concentration model for NFW profile. Can be "direct_from_halo_catalog" or "dutton_maccio14".
        mdef: str
            halo definition to use (e.g., "vir" for virial and "200c" for 200 times the critical density)
        calculate_xi: boolean
            whether to calculate spatial correlation function or not. Defaults to True.
        calculate_acf: boolean
            whether to calculate angular correlation function or not. Defaults to True.
        calculate_covariance: boolean
            whether to calculate covariance of xi or not. Defaults to False.
        njacksubdivisions: int
            number of subdivisions on box to make when creating jackknife samples, that is,
            the number of jackknife samples is njacksubdivisions**3


    Returns:
        Nothing
    """
    theta_bins = np.logspace(np.log10(theta_min), np.log10(theta_max), ntheta_bins)

    if sim_redshifts is None:
        sim_redshift = np.zeros_like(hodpars)
    if fnames is None:
        fnames = [None for _ in hodpars]

    model = None
    for hod, sim_redshift, fname in zip(hodpars, sim_redshifts, fnames):
        base_filename = "{}_z{:02.0f}_sigmaM_{:02.0f}_massMin_{:02.0f}_alpha{:02.0f}_mass0_{:02.0f}_mass1_{:02.0f}_fcentral_{:02.0f}".format(sim, sim_redshift*100, hod["sigmam"]*100, hod["massmin"]*100, hod["alpha"]*100, hod["mass0"]*100, hod["mass1"]*100, hod["fcentral"]*100)

        print("HOD parameters: ", hod)
        print("Populating halos...")
        if fname is None:
            gals, model = populate_halos(hodpars=hod, sim=sim, sim_redshift=sim_redshift,
                                         halo_finder=halo_finder, model=model, mdef=mdef,
                                         conc_mass_model=conc_mass_model)
        else:
            gals, model = populate_halos(hodpars=hod, fname=fname, model=model, mdef=mdef,
                                         conc_mass_model=conc_mass_model)
        gals_file = os.path.join(data_dir, "gals_mock_" + base_filename + ".csv")
        gals.write(gals_file, format="csv")

        if calculate_acf:
            print("Calculating ACF...")
            theta, acf, catalog = calculate_mock_acf(theta_bins, model.mock.Lbox, gals, nrandoms=nrandoms)
            acf_data = np.c_[theta, acf]
            acf_file = os.path.join(data_dir, "acf_mock_" + base_filename + ".csv")
            acf_header = "theta,acf"
            np.savetxt(fname=acf_file, X=acf_data, header=acf_header, delimiter=",")
            del(acf_data, theta, acf)
        else:
            coords = np.vstack([gals['x'], gals['y'], gals['z']]).T
            vels = np.vstack([gals['vx'], gals['vy'], gals['vz']]).T
            ra, dec, z = mock_survey.ra_dec_z(coords, vels)
            ra = np.degrees(ra)
            dec = np.degrees(dec)
            catalog = np.c_[ra, dec, z]
        catalog_file = os.path.join(data_dir, "catalog_mock_" + base_filename + ".csv")
        catalog_header = "ra[deg],theta[deg],z"
        np.savetxt(fname=catalog_file, X=catalog, header=catalog_header, delimiter=",")
        del(catalog)

        if calculate_xi:
            print("Calculating xi(r) and covariances...")
            if calculate_covariance:
                r, xi, xi_cov = calculate_mock_xi(gals, r_min, r_max, nr_bins, model.mock.Lbox,
                                                  njacksubdivisions=njacksubdivisions,
                                                  return_halo_decomp=False,
                                                  return_covariance=True)
                del(gals)
                xi_data = np.c_[r, xi]
                xi_file = os.path.join(data_dir, "xi_jack_" + base_filename + ".csv")
                xi_header = "r,xi"
                np.savetxt(fname=xi_file, X=xi_data, header=xi_header, delimiter=",")
                del(xi_data, r, xi)
                xi_cov_file = os.path.join(data_dir, "cov_xi_mock_" + base_filename + ".csv")
                np.savetxt(fname=xi_cov_file, X=xi_cov, delimiter=",")
                del(xi_cov)

            else:
                r, xi, xi_1h, xi_2h = calculate_mock_xi(gals, r_min, r_max, nr_bins, model.mock.Lbox,
                                                        return_halo_decomp=True, return_covariance=False)
                del(gals)
                xi_data = np.c_[r, xi, xi_1h, xi_2h]
                xi_file = os.path.join(data_dir, "xi_mock_" + base_filename + ".csv")
                xi_header = "r,xi,xi_1h,xi_2h"
                np.savetxt(fname=xi_file, X=xi_data, header=xi_header, delimiter=",")
                del(xi_data, r, xi, xi_1h, xi_2h)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("halotools_utils")
    parser.add_argument("-configfile", help="path to file holding hod parameters to produce mocks.")
    args = parser.parse_args()
    configfile = args.configfile
    if configfile is None:
        configfile = "parameters.ini"
        if not os.path.isfile(configfile):
            print("Error: Configuration file holding hod parameters not found.", file=sys.stderr)
            exit(1)
    config = ConfigParser()
    config.read(configfile)


    section = "hod_parameters"
    hod_parameters_names = ["fcentral", "massmin", "mass1", "mass0", "sigmam", "alpha"]
    hod_parameters_list = [config.get(section, name).split() for name in hod_parameters_names]

    hod_parameters_dicts = []
    for parameters in product(*hod_parameters_list):
        hod_parameters_dicts.append({name: float(value) for name,value in zip(hod_parameters_names, parameters)})


    section = "simulation_parameters"
    try:
        fnames = config.get(section, "fnames").split()
        fnames = [os.path.abspath(fname) for fname in fnames]
    except:
        fnames = None
    sim_name = config.get(section, "sim_name", fallback="bolshoi")
    halo_finder = config.get(section, "halo_finder", fallback="rockstar")
    redshifts = [float(z) for z in config.get(section, "redshifts").split()]
    conc_mass_model = config.get(section, "conc_mass_model", fallback="direct_from_halo_catalog")
    halo_definition = config.get(section, "halo_definition", fallback="vir")

    section = "output_parameters"
    rmin = config.getfloat(section, "r_min", fallback=0.01)
    rmax = config.getfloat(section, "r_max", fallback=50.)
    rbins = config.getint(section, "r_bins", fallback=50)
    calculate_xi = config.getboolean(section, "calculate_xi", fallback=True)

    thetamin = config.getfloat(section, "theta_min", fallback=0.1)
    thetamax = config.getfloat(section, "theta_max", fallback=10.)
    thetabins = config.getint(section, "theta_bins", fallback=50)
    calculate_acf = config.getboolean(section, "calculate_acf", fallback=True)

    calculate_covariance = config.getboolean(section, "calculate_covariance", fallback=False)
    njacksubdivisions = config.getint(section, "njacksubdivisions", fallback=5)

    output_dir = os.path.abspath(config.get(section, "output_dir", fallback="./mock-catalogs/"))

    hod_list = []
    redshift_list = []

    for z in redshifts:
        for hod in hod_parameters_dicts:
            redshift_list.append(z)
            hod_list.append(hod)

    hod_list = np.array(hod_list)
    redshift_list = np.array(redshift_list)

    generate_mocks(hod_list, rmin, rmax, rbins, thetamin, thetamax, thetabins,
                   output_dir, sim=sim_name,
                   sim_redshifts=redshift_list, fnames=fnames,
                   conc_mass_model=conc_mass_model, mdef=halo_definition,
                   calculate_xi=calculate_xi, calculate_acf=calculate_acf,
                   calculate_covariance=calculate_covariance,
                   njacksubdivisions=njacksubdivisions)

